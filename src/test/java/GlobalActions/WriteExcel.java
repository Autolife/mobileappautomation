/*
 * Added by Vinudeep for Mobile Automation Testing
 */

/*
 * Added by Vinudeep for Mobile Automation Testing
 */

package GlobalActions;

import helpers.Filereadingutility;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class WriteExcel {


    public static void main(String[] args) throws IOException {


                 //   private static final String File_name =   "Configurations\\testData\\TestResults.xlsx";
            String relativePath = System.getProperty("user.dir");
            System.out.println("user.dir is " + relativePath);
            String EnvPropFilePath = relativePath + "/Configurations/Properties/AppConfig.properties";
            System.out.println("The Env prop path is " + EnvPropFilePath);

            String excelSheetName = Filereadingutility.getPropertyValue(EnvPropFilePath, "excelSheetName");
            System.out.println("The App which will be used for testing is " + excelSheetName);
            File File_name = new File("Configurations\\testData\\" + excelSheetName);
            System.out.println("Excel sheet reference is given");

            XSSFWorkbook workbook = new XSSFWorkbook();
            // XSSFSheet sheet1 = wb1.createSheet("Results");
            XSSFSheet sheet = workbook.createSheet("Results");
            Row row = sheet.createRow(0);
            Cell cell = row.createCell(1);
            cell.setCellValue("OrderNumber for journey is" + timestamp());
            //  cell.setCellValue("SoftwareTestingMaterial.com");
            FileOutputStream fos = new FileOutputStream(File_name);
            workbook.write(fos);
            fos.close();
            System.out.println("END OF WRITING DATA IN EXCEL");
        }



    private static String timestamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
    }

}
