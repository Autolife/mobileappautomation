/*
 * Added by Vinudeep for Mobile Automation Testing
 */

package GlobalActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ReadEmail {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "Browsers\\ChromeDriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://mail.google.com/mail/?tab=wm");

        Thread.sleep(4000);

        // gmail login

        driver.findElement(By.xpath("//input[@type='email']")).sendKeys("vinudeep.malalur");

        //Clicking on next button

        driver.findElement(By.xpath("//div[@role='button'][@id='identifierNext']")).click();

        Thread.sleep(3000);
        //Entering password

        driver.findElement(By.xpath("//input[@type='password']")).sendKeys("flash3star***");

        //Clicking on next button

        driver.findElement(By.xpath("//div[@role='button'][@id='passwordNext']")).click();

        Thread.sleep(6000);

        // Considering un-read email form inbox into a list

        List<WebElement> unreadEmail = driver.findElements(By.xpath("//*[@class='zF']"));

        // Mailer name for which i want to check do i have an email in my inbox
        String MyMailer = "udacity";

        //Logic

        for(int i=0;i<unreadEmail.size(); i++)
        {
            if(unreadEmail.get(i).isDisplayed()==true){
                //verify if you have got mail form a specific mailer (Note Un-read mails)
                if(unreadEmail.get(i).getText().equalsIgnoreCase(MyMailer));
                    System.out.println("Yes we have got mail form " + MyMailer);
                    System.out.println(unreadEmail.get(i).getText().equalsIgnoreCase(MyMailer));
                // also you can perform more actions here
                // like if you want to open email form the mailer
                break;
            }else{
                System.out.println("No mail form " + MyMailer);
            }

            }
            driver.quit();
        }

    }

